<?xml version="1.0" encoding="UTF-8"?>
<xsd:schema
	xmlns:xsd="http://www.w3.org/2001/XMLSchema"
	targetNamespace="http://campus.en.kku.ac.th"
	xmlns="http://campus.en.kku.ac.th"
	elementFormDefault="qualified">
	<xsd:element name="nation"
		type="nationType"/>
	<xsd:complexType name="nationType">
		<xsd:sequence>
			<xsd:element name="name"
				type="xsd:string"/>
			<xsd:element name="location"
				type="xsd:string"/>
		</xsd:sequence>
	<xsd:attribute name="id" type="xsd:ID"/>
	</xsd:complexType>
</xsd:schema>