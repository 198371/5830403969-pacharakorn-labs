/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab4xml;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;
import java.io.IOException;

public class DOMTraversal {
    @SuppressWarnings({"UseSpecificCatch", "CallToPrintStackTrace"})
    public static void main(String[] args) {
        try {
        File fXmlFile = new File("src/lab4xml/nation.xml");
	DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	Document doc = dBuilder.parse(fXmlFile);
        doc.getDocumentElement().normalize();
        Element root = doc.getDocumentElement();
        
        followNode(root);
        } catch (Exception e) {
	e.printStackTrace();
        }  
    }
    
    public static void followNode(Node node) throws IOException {
        nameNode(node);
        if (node.hasChildNodes()) {
            String name = node.getNodeName();
            int numChildren = node.getChildNodes().getLength();
            System.out.println("node " + name + " has " + numChildren
                               + " children");
            Node firstChild = node.getFirstChild();
            followNode(firstChild);
        }
        Node nextNode = node.getNextSibling();
        if (nextNode != null) followNode(nextNode);
    }
    public static void nameNode(Node node){
         System.out.println("Node:type is "+ selectNode(node) +" name is " + node.getNodeName() + 
                           " value is " + node.getNodeValue());
    }
    
     public static String selectNode(Node node){
        if(node.getNodeType() == 1){
             return "element";
        } else 
            return "text";
     }
}
