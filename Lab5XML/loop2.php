<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $xmldoc = new DOMDocument();
        $xmldoc->load("nation.xml");
        $nation = $xmldoc->documentElement;
        process_shelf($nation);

        function process_shelf($nodes) {
            echo "Node type is ", text_child($nodes), " name is ", $nodes->nodeName,
            " value is ", $nodes->nodeValue, "<br />";
            echo "node ", $nodes->nodeName, " has ", num_child($nodes), " children<br/>";
            foreach ($nodes->childNodes as $item) {
                echo "Node type is ", text_child($item), " name is ", $item->nodeName,
                " value is ", $item->nodeValue, "<br />";
                if ($item->hasChildNodes()) {
                    foreach ($item->childNodes as $node) {
                        echo "node ", $item->nodeName, " has ", num_child($item), " children<br/>";
                        echo "Node type is ", text_child($node), " name is ", $node->nodeName,
                        " value is ", $node->nodeValue, "<br />";
                    }
                }
            }
        }

        function num_child($node) {
            $num = 0;
            foreach ($node->childNodes as $nodes) {
                $num = $num + 1;
            }
            return $num;
        }
        
        function text_child($node) {
            if($node->nodeType == 1){
                return "element";
            } else {
                return "text";
            }
        }
        
        ?>
    </body>
</html>