<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $fileName = "myprofile.xml";
        $doc = new DomDocument('1.0');
// create element 'nation'
        $root = $doc->createElement('profile');
// assign the element 'nation' as the root node of the XMLdoc
        $doc->appendChild($root);
// set attribute of the root node
        $root->setAttribute('id', '5830403969');
        $major = $doc->createElement('major');
        $majorValue = $doc->createTextNode('Computer Engineering');
        $major->appendChild($majorValue);
        $area = $doc->createElement('area');
        $areaValue = $doc->createTextNode('Software');
        $area->appendChild($areaValue);
        $name = $doc->createElement('name');
        $nameValue = $doc->createTextNode('Pacharakorn');
        $name->appendChild($nameValue);
        $major->appendChild($area);
// make the element node 'name' be the child of the root node of XMLdoc
        $root->appendChild($major);
// make the element node 'location' be the child of root node of XML doc
        $root->appendChild($name);
// save DOM tree in an XML file
//$str = $doc->saveXML();
        $doc->save($fileName);
        echo "Finish writing file $fileName";
        ?>
    </body>
</html>
